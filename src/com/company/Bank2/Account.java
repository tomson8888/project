package com.company.Bank2;

public class Account {


    private int number_account ;
    private int starting_balance ;
    private String stateOfAccount;

    private int final_balance ;

    private int summaForTransfer;

    private String name;
    private String surname;

    Account(int number_account,int starting_balance,String stateOfAccount, String name, String surname) {
        this.number_account=number_account;
        this.starting_balance = starting_balance;
        this.stateOfAccount = stateOfAccount;
        this.name = name;
        this.surname = surname;
    }



    public int getNumber_account() {
        return number_account;
    }

    public void setNumber_account(int number_account) {
        this.number_account = number_account;
    }

    public int getStarting_balance() {
        return starting_balance;
    }

    public void setStarting_balance(int starting_balance) {
        this.starting_balance = starting_balance;
    }

    public String getStateOfAccount() {
        return stateOfAccount;
    }

    public void setStateOfAccount(String stateOfAccount) {
        this.stateOfAccount = stateOfAccount;
    }

    public int getFinal_balance() {
        return final_balance;
    }

    public void setFinal_balance(int final_balance) {
        this.final_balance = final_balance;
    }

    public int getSummaForTransfer() {
        return summaForTransfer;
    }

    public void setSummaForTransfer(int summaForTransfer) {
        this.summaForTransfer = summaForTransfer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
