package com.company.Bank2;

public interface AccountServis {
     void withdraw();
     void deposit();
     void transferFromAccToAcc();
}
