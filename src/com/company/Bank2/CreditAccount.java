package com.company.Bank2;

public class CreditAccount extends Account {
    private int creditPercent ;

    CreditAccount(int creditPercent){
        super(407,47,"Open", "Max", "Sidorov");
        this.creditPercent = creditPercent;
    }

    public int getCreditPercent() {
        return creditPercent;
    }

    public void setCreditPercent(int creditPercent) {
        this.creditPercent = creditPercent;
    }
}
