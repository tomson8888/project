package com.company.Bank2;
import java.util.Scanner;

public class WithdrawAndDepositCreditAccount implements AccountServis {

    Scanner scanner = new Scanner(System.in);

    CreditAccount creditAcc = new CreditAccount(2);
    BusinessAccount businessAcc = new BusinessAccount();

    float percentage;
    float balanceAfterCommission;

    @Override
    public void withdraw() {
        System.out.println("Введите сумму списания для кредитового счета,за списание будет взиматься коммисия 2%");
        String sumCre = scanner.nextLine();
        if (businessAcc.getStarting_balance() <= 0 ){
            System.out.println("Списание не возможно тк баланс меньше или равен нулю");
        }else {
            creditAcc.setFinal_balance(creditAcc.getStarting_balance() - Integer.parseInt(sumCre));
            percentage = ((Float.parseFloat(String.valueOf(creditAcc.getFinal_balance())) * Float.parseFloat(String.valueOf(creditAcc.getCreditPercent()))) / 100);
            balanceAfterCommission = Float.parseFloat(String.valueOf(creditAcc.getFinal_balance())) - percentage;
            System.out.println("Остаток на счете до снятия % " + creditAcc.getFinal_balance() + " Номер счета " + creditAcc.getNumber_account() + " Состояние счета "
                    + creditAcc.getStateOfAccount() + " Начальный баланс " + creditAcc.getStarting_balance() + " Процент комиссии " + creditAcc.getCreditPercent() + "% " + " Комиссия за снятие " + percentage);
            System.out.println("Остаток насчете после снятия " + Float.parseFloat(String.valueOf(balanceAfterCommission)));
        }
    }

    @Override
    public void deposit() {
        System.out.println("Введите сумму пополнения для кредитового счета");
        String sumCre = scanner.nextLine();
        if (creditAcc.getStateOfAccount().equals("Close")) {
            System.out.println("Счет закрыт, списание невозможно");
        } else if (creditAcc.getStateOfAccount().equals("Blocked")) {
            System.out.println("Счет заблокирован, обратитесь в банк для разблокировки счета");
        } else if (creditAcc.getStateOfAccount().equals("Open")) {
            if (businessAcc.getStarting_balance() <= 0) {
                System.out.println("Списание не возможно тк балан меньше или равен нулю");
            } else {
                creditAcc.setFinal_balance(creditAcc.getStarting_balance() + Integer.parseInt(sumCre));
                System.out.println("Остаток на счете" + creditAcc.getFinal_balance() + " Номер счета " + creditAcc.getNumber_account() + " Состояние счета "
                        + creditAcc.getStateOfAccount() + " Начальный баланс " + creditAcc.getStarting_balance());
            }
        }
    }

    @Override
    public void transferFromAccToAcc() {
        System.out.println("Введите сумму для перевода с кредитового счета на бизнес счет");
        String sumTransf = scanner.nextLine();
        //списание с кретитового счет
        creditAcc.setFinal_balance(creditAcc.getStarting_balance() - Integer.parseInt(sumTransf));
        // подсчет процентов
        percentage = ((Float.parseFloat(String.valueOf(creditAcc.getFinal_balance())) * Float.parseFloat(String.valueOf(creditAcc.getCreditPercent()))) / 100);
        // списание процентов со счета
        balanceAfterCommission = Float.parseFloat(String.valueOf(creditAcc.getFinal_balance() - percentage));
        //пополнение бизнес счета
        businessAcc.setSummaForTransfer(Integer.parseInt(sumTransf) + businessAcc.getStarting_balance());
        System.out.println("Остаток на кредитовом счете до снятия % " + creditAcc.getFinal_balance());
        System.out.println("Коммиссия за перевод средств составила " + percentage);
        System.out.println("Остаток на кредитовом счете после сняти % " + balanceAfterCommission);
        System.out.println("Суммма перевода " + sumTransf + " Остаток на бизнес счете " + businessAcc.getSummaForTransfer());
    }

}
