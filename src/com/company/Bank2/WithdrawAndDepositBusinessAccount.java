package com.company.Bank2;
import java.util.Scanner;

public class WithdrawAndDepositBusinessAccount implements AccountServis {

    BusinessAccount businessAcc = new BusinessAccount();
    CreditAccount creditAcc = new CreditAccount(0);
    Scanner scanner = new Scanner(System.in);

    @Override
    public void withdraw() {
        System.out.println("Введите сумму списания для бизнес счета");
        String summBus = scanner.nextLine();
        if (businessAcc.getStateOfAccount().equals("Close")) {
            System.out.println("Счет закрыт, пополнение невозможно");
        } else if (businessAcc.getStateOfAccount().equals("Blocked")) {
            System.out.println("Счет заблокирован, обратитесь в банк для разблокировки счета");
        } else if (businessAcc.getStateOfAccount().equals("Open")) {
            if (businessAcc.getStarting_balance() <= 0) {
                System.out.println("Списание не возможно тк балан меньше или равен нулю");
            } else {
                businessAcc.setFinal_balance(businessAcc.getStarting_balance() - Integer.parseInt(summBus));
                System.out.println("Остаток на счете" + businessAcc.getFinal_balance() + " Номер счета " + businessAcc.getNumber_account() + " Состояние счета "
                        + businessAcc.getStateOfAccount() + " Начальный баланс " + businessAcc.getStarting_balance());
            }
        }
    }

    @Override
    public void deposit() {
        System.out.println("Введите сумму пополнения для бизнес счета");
        String summBus = scanner.nextLine();
        if (businessAcc.getStateOfAccount().equals("Close")) {
            System.out.println("Счет закрыт, списание невозможно");
        } else if (businessAcc.getStateOfAccount().equals("Blocked")) {
            System.out.println("Счет заблокирован, обратитесь в банк для разблокировки счета");
        } else if (businessAcc.getStateOfAccount().equals("Open")) {
            businessAcc.setFinal_balance(businessAcc.getStarting_balance() + Integer.parseInt(summBus));
            System.out.println("Остаток на счете" + businessAcc.getFinal_balance() + " Номер счета " + businessAcc.getNumber_account() + " Состояние счета "
                    + businessAcc.getStateOfAccount() + " Начальный баланс " + businessAcc.getStarting_balance());
        }
    }

        @Override
        public void transferFromAccToAcc () {
            System.out.println("Введите сумму для перевода с бизнес счета на кредитовый счет");
            String sumTransf = scanner.nextLine();
            businessAcc.setFinal_balance(businessAcc.getStarting_balance() - Integer.parseInt(sumTransf));
            creditAcc.setSummaForTransfer(Integer.parseInt(sumTransf) + creditAcc.getStarting_balance());
            System.out.println("Кредитовйы счет баланс " + creditAcc.getSummaForTransfer());
            System.out.println("Остаток на бизнес счете " + businessAcc.getFinal_balance());
            System.out.println("Суммма перевода " + sumTransf + " Остаток на бизнес счете " + businessAcc.getFinal_balance() + "Остаток на кредитовом счете "
                    + creditAcc.getSummaForTransfer());
        }

}