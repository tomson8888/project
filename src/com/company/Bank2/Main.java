package com.company.Bank2;

import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();
        main.action();

    }

    Scanner scanner = new Scanner(System.in);
    WithdrawAndDepositBusinessAccount wdba = new WithdrawAndDepositBusinessAccount();
    WithdrawAndDepositCreditAccount wdca = new WithdrawAndDepositCreditAccount();

    public void action(){
        System.out.println("Введи действие которое вы хотите сделать над счетом: пополнить '+', списать '-'");
        System.out.println("Перевод со счета на счет 'T'");
        System.out.println("Записать информацию по клиентам 'W'");
        String action = scanner.nextLine();
        switch (action){
            case "+":
                System.out.println("Номера счетов 408 и 407");
                System.out.println("Введите номер счета");
                String numAccDep = scanner.nextLine();
                if (numAccDep.trim().isEmpty()) {
                    System.out.println("Строка не содержит никакого значения");
                }else {
                    if (Integer.parseInt(numAccDep) == 408) {
                        wdba.deposit();
                    } else if (Integer.parseInt(numAccDep) == 407) {
                        wdca.deposit();
                    }else {
                        System.out.println("Введен не верный счет");
                    }
                }
                break;
            case "-":
                System.out.println("Номера счетов 408 и 407");
                System.out.println("Введите номер счета");
                String numAccWith = scanner.nextLine();
                if (numAccWith.trim().isEmpty()){
                    System.out.println("Строка не содержит никакого значения");
                }else {
                    if (Integer.parseInt(numAccWith) == 408) {
                        wdba.withdraw();
                    }else if(Integer.parseInt(numAccWith) == 407){
                        wdca.withdraw();
                    }else {
                        System.out.println("Введен не верный счет");
                    }
                }
                break;
            case "T":
                System.out.println("Введите номер счета c котрого хотите перевести средства");
                String numAccTrans = scanner.nextLine();
                if (numAccTrans.trim().isEmpty()){
                    System.out.println("Строка не содержит никакого значения");
                }else {
                    if (Integer.parseInt(numAccTrans) == 408){
                        System.out.println("Выбран бизнес счет");
                        wdba.transferFromAccToAcc();
                    }else if (Integer.parseInt(numAccTrans) == 407) {
                        System.out.println("Выбран кредитовй счет");
                        wdca.transferFromAccToAcc();
                    }
                }
                break;
            case "W":
                System.out.println("Запись информации по клиентам в файл");
                String info;
                info = String.valueOf(wdba.businessAcc.getSurname()+" "+wdba.businessAcc.getName()+" Номер счета : "+wdba.businessAcc.getNumber_account()+
                        " Баланс на счете : "+wdba.businessAcc.getStarting_balance()+" Состояние счета : "+wdba.businessAcc.getStateOfAccount())+" \n"+
                wdca.creditAcc.getSurname()+" "+wdca.creditAcc.getName()+" Номер счета : "+wdca.creditAcc.getNumber_account()+
                        " Баланс на счете : "+wdca.creditAcc.getStarting_balance()+" Состояние счета : "+wdca.creditAcc.getStateOfAccount();
                try {
                    FileOutputStream fos = new FileOutputStream("client.txt");
                    byte[] buffer = info.getBytes();
                    try {
                        fos.write(buffer,0,buffer.length);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                break;
            default:
                System.out.println("Операция не распознана.");
        }
    }
}
